import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class NeuralNetwork {

    private List<Layer> layers;
    
    public NeuralNetwork(Integer input, Integer output, Function<Double,Double> activationFunction) {
        layers = new ArrayList<Layer>();
    }

    public List<Layer> getLayers() {
        return layers;
    }

    public void setLayers(List<Layer> layers) {
        this.layers = layers;
    }
    
}
